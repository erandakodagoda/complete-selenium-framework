package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *  @author : Eranda Kodagoda
 *  @date : August 10, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

public class HomePageObjects {
    WebDriver driver;

    public HomePageObjects(WebDriver driver){
        this.driver=driver;
    }

    By signInMenu = By.id("nav-item-signin");
    By createMenuItem = By.xpath("//*[@id='nav-item-create']/a");

    public WebElement getSignInMenu (){
        return driver.findElement(signInMenu);
    }
    public WebElement getCreateMenuItem(){
        return driver.findElement(createMenuItem);
    }

}
