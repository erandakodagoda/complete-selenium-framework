$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("01-Registration.feature");
formatter.feature({
  "line": 1,
  "name": "As a Pastbook User I should be able to sign up to the application by providing email and I should be able to reset the password",
  "description": "",
  "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "As a user I should be able to Sign up and reset Password",
  "description": "",
  "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I click on sign in menu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I navigated to sign in popup screen",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on use email option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter the \u003cemail\u003e to the email text box",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I get re-directed to Home Page as a Logged in User",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I navigated to profile",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I reset the password to \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on save password button",
  "keyword": "Then "
});
formatter.examples({
  "line": 17,
  "name": "",
  "description": "",
  "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password;",
  "rows": [
    {
      "cells": [
        "email",
        "password"
      ],
      "line": 18,
      "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password;;1"
    },
    {
      "cells": [
        "testpastbook+14@testemail.com",
        "123@com"
      ],
      "line": 19,
      "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I am in Pastbook Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonSteps.i_am_in_Pastbook_Home_Page()"
});
formatter.result({
  "duration": 21504102500,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "As a user I should be able to Sign up and reset Password",
  "description": "",
  "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I click on sign in menu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I navigated to sign in popup screen",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on use email option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter the testpastbook+14@testemail.com to the email text box",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I get re-directed to Home Page as a Logged in User",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I navigated to profile",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I reset the password to 123@com",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on save password button",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonSteps.i_click_on_sign_in_menu()"
});
formatter.result({
  "duration": 133597100,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_navigated_to_sign_in_popup_screen()"
});
formatter.result({
  "duration": 7003085600,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_use_email_option()"
});
formatter.result({
  "duration": 107494400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testpastbook+14@testemail.com",
      "offset": 12
    }
  ],
  "location": "CommonSteps.i_enter_the_test_email_com_to_the_email_text_box(String)"
});
formatter.result({
  "duration": 116680000,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 67667200,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_get_re_directed_to_Home_Page_as_a_Logged_in_User()"
});
formatter.result({
  "duration": 30263884500,
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for element found by By.xpath: //*[@class\u003d\u0027container\u0027]/h2 to have text \"You are in! Ready to go.\". Current text: \"null\" (tried for 30 second(s) with 500 milliseconds interval)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027EYEPAXLT-109\u0027, ip: \u0027192.168.91.32\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_261\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 85.0.4183.121, chrome: {chromedriverVersion: 85.0.4183.87 (cd6713ebf92fa..., userDataDir: C:\\Users\\eranda_k\\AppData\\L...}, goog:chromeOptions: {debuggerAddress: localhost:50946}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 5301783b6331f25b88f5f472b5b83f23\r\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:95)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:272)\r\n\tat stepDefinitions.CommonSteps.i_get_re_directed_to_Home_Page_as_a_Logged_in_User(CommonSteps.java:87)\r\n\tat ✽.And I get re-directed to Home Page as a Logged in User(01-Registration.feature:12)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "CommonSteps.iNavigatedToProfile()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "123@com",
      "offset": 24
    }
  ],
  "location": "CommonSteps.iResetThePasswordToPassword(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonSteps.iClickOnSavePasswordButton()"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("02-CreatePastBookAlbum.feature");
formatter.feature({
  "line": 1,
  "name": "As a User I should be able to Create a PastBook",
  "description": "",
  "id": "as-a-user-i-should-be-able-to-create-a-pastbook",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "As a Logged in User I should be able to create a PastBook",
  "description": "",
  "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I click on sign in menu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I navigated to sign in popup screen",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on use email option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter the \u003cemail\u003e to the email text box",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I enter the \u003cpassword\u003e on the password text box",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I get re-directed to Home Page as a Logged in User",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on create menu option",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I get navigated to create Past Book Page",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I close pop-up advertisement",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I enter \u003ctitle\u003e into Title text field",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I click on option link text",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I click on Add Cover Button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I add a cover photo \u003cimg\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I click on create your Pastbook button",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "I click on upload picture",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "I add album photo \u003cimg1\u003e and click on upload",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I click on continue button",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I logout from pastbook application",
  "keyword": "And "
});
formatter.examples({
  "line": 28,
  "name": "",
  "description": "",
  "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook;",
  "rows": [
    {
      "cells": [
        "email",
        "password",
        "title",
        "img",
        "img1"
      ],
      "line": 29,
      "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook;;1"
    },
    {
      "cells": [
        "testpastbook+14@testemail.com",
        "123@com",
        "My Test Past Book",
        "7.jpg",
        "3.jpg"
      ],
      "line": 30,
      "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I am in Pastbook Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonSteps.i_am_in_Pastbook_Home_Page()"
});
formatter.result({
  "duration": 7893808900,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "As a Logged in User I should be able to create a PastBook",
  "description": "",
  "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I click on sign in menu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I navigated to sign in popup screen",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on use email option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter the testpastbook+14@testemail.com to the email text box",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I enter the 123@com on the password text box",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I get re-directed to Home Page as a Logged in User",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on create menu option",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I get navigated to create Past Book Page",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I close pop-up advertisement",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I enter My Test Past Book into Title text field",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I click on option link text",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I click on Add Cover Button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I add a cover photo 7.jpg",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I click on create your Pastbook button",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "I click on upload picture",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "I add album photo 3.jpg and click on upload",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I click on continue button",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I logout from pastbook application",
  "keyword": "And "
});
formatter.match({
  "location": "CommonSteps.i_click_on_sign_in_menu()"
});
formatter.result({
  "duration": 577927500,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_navigated_to_sign_in_popup_screen()"
});
formatter.result({
  "duration": 8018267300,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_use_email_option()"
});
formatter.result({
  "duration": 69876400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testpastbook+14@testemail.com",
      "offset": 12
    }
  ],
  "location": "CommonSteps.i_enter_the_test_email_com_to_the_email_text_box(String)"
});
formatter.result({
  "duration": 95977300,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 55572000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123@com",
      "offset": 12
    }
  ],
  "location": "CommonSteps.i_enter_the_password_text_box(String)"
});
formatter.result({
  "duration": 1137493100,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 37578500,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_get_re_directed_to_Home_Page_as_a_Logged_in_User()"
});
formatter.result({
  "duration": 5859572800,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_create_menu_option()"
});
formatter.result({
  "duration": 59757300,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_get_navigated_to_create_book_page()"
});
formatter.result({
  "duration": 1958727100,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.iClosePopUpAdvertisement()"
});
formatter.result({
  "duration": 94206500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "My Test Past Book",
      "offset": 8
    }
  ],
  "location": "CommonSteps.i_enter_title_into_field(String)"
});
formatter.result({
  "duration": 90665200,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_option_link_text()"
});
formatter.result({
  "duration": 49754200,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"link text\",\"selector\":\"Options Â»\"}\n  (Session info: chrome\u003d85.0.4183.121)\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027EYEPAXLT-109\u0027, ip: \u0027192.168.91.32\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_261\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 85.0.4183.121, chrome: {chromedriverVersion: 85.0.4183.87 (cd6713ebf92fa..., userDataDir: C:\\Users\\eranda_k\\AppData\\L...}, goog:chromeOptions: {debuggerAddress: localhost:51097}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 61af83ed07bdf776518a78a70409905d\n*** Element info: {Using\u003dlink text, value\u003dOptions Â»}\r\n\tat sun.reflect.GeneratedConstructorAccessor13.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByLinkText(RemoteWebDriver.java:380)\r\n\tat org.openqa.selenium.By$ByLinkText.findElement(By.java:220)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\r\n\tat pageObjects.CreatePastBookPageObjects.getOptionLinkTxt(CreatePastBookPageObjects.java:37)\r\n\tat stepDefinitions.CommonSteps.i_click_on_option_link_text(CommonSteps.java:107)\r\n\tat ✽.And I click on option link text(02-CreatePastBookAlbum.feature:19)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_add_cover_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "7.jpg",
      "offset": 20
    }
  ],
  "location": "CommonSteps.i_add_a_cover_photo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonSteps.i_click_on_create_pastbook_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonSteps.iClickOnUploadPicture()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "3.jpg",
      "offset": 18
    }
  ],
  "location": "CommonSteps.iAddAlbumPhotoImgAndImgAndClickOnUpload(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonSteps.iClickOnContinueButton()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonSteps.iSeePublishedBookPage()"
});
formatter.result({
  "status": "skipped"
});
});